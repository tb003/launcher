package com.tbs.pad;

//import android.app.Activity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.Toast;

import com.phonegap.DroidGap;
import com.tbs.pad.R;

public class PadasalaLauncher extends Activity {
    /** Called when the activity is first created. */
	
	final int LEMONS_ID = 1;
	final int AAJAR_ID =2;
	final int BOATS_ID =3;
	final int COGNITE_ID =4;
	final int CLARITY_ID =5;
	final int NEURON_ID =6;
	final int FACEBOOK_ID =7;
	final int SKYPE_ID =8;
	final int TWITTER_ID =9;
	final int LINKED_ID =10;
	final int SEED_ID =11;
	final int DRONA_ID =12;
	final int DARSHAN_ID =13;
	final int SETTINGS_ID =14;
	final int HELP_ID =15;
	final int ABOUTUS_ID =16;
	final int BLOOMBERG_ID=17;
	
	
    WebView browser = null;
    String URL = "file:///android_asset/www/index.html";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
      //  super.loadUrl("file:///android_asset/www/index.html");
        
        browser = (WebView) findViewById(R.id.webview);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.addJavascriptInterface(new JavaScriptInterface(this), "Android");
        
        browser.loadUrl(URL);
    
        
        
    }
    public class JavaScriptInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        	//browser.loadUrl("file:///www.google.com");
            PackageManager packageManager = getPackageManager();
            Intent skype = packageManager.getLaunchIntentForPackage("com.skype.raider");
            skype.setData(Uri.parse("tel:65465446"));
            startActivity(skype);
        }
        
        public void openApplication(int app){
        	//Toast.makeText(mContext, "dennis", Toast.LENGTH_SHORT).show();
        	
        	switch (app){
        
        	case LEMONS_ID:
        	{
        		PackageManager lemonsManager = getPackageManager();
            	Intent lemons = lemonsManager.getLaunchIntentForPackage("com.thinkbox.padasala.LeMonSlocal");
                //lemons.setData(Uri.parse("tel:65465446"));
                startActivity(lemons);
                break;
        	}
        	
        	
        	case AAJAR_ID:
        	{
        		PackageManager aajarManager = getPackageManager();
            	Intent aajar =aajarManager.getLaunchIntentForPackage("org.alljoyn.bus.sample.chat");
                startActivity(aajar);
                break;
        	}
        	
        	case DRONA_ID:
        	{
        		PackageManager dronaManager = getPackageManager();
            	Intent drona = dronaManager.getLaunchIntentForPackage("org.thinkbox.zlibrary.ui.android");
                startActivity(drona);
                break;
        	}
        	
        	
        	case COGNITE_ID:
        	{
        		PackageManager cogniteManager = getPackageManager();
            	Intent cognite = cogniteManager.getLaunchIntentForPackage("com.ichi2.anki");
                startActivity(cognite);
                break;
        	}
        	
        	
        	case CLARITY_ID:
        	{
        		PackageManager clarityManager = getPackageManager();
            	Intent clarity =clarityManager.getLaunchIntentForPackage("com.tb.ram");
                startActivity(clarity);
                break;
        	}
        	
        	
        	case NEURON_ID:
        	{
        		PackageManager neuronManager = getPackageManager();
            	Intent neuron= neuronManager.getLaunchIntentForPackage("com.thinkbox.padasala.neuron");
                startActivity(neuron);
                break;
        	}
        	
        	
        	case SKYPE_ID:
        	{
        		PackageManager skypeManager = getPackageManager();
            	Intent skype= skypeManager.getLaunchIntentForPackage("com.facebook.katana");
                startActivity(skype);
                break;
        	}
        	
        	
        	case FACEBOOK_ID:
        	{
        		PackageManager facebookManager = getPackageManager();
            	Intent facebook = facebookManager.getLaunchIntentForPackage("com.skype.android.lite");
                startActivity(facebook);
                break;
        	}
        	
        	
        	case TWITTER_ID:
        	{
        		PackageManager twitterManager = getPackageManager();
            	Intent twitter= twitterManager.getLaunchIntentForPackage("com.twitter.android");
                startActivity(twitter);
                break;
        	}
        	
        	
        	
        	case LINKED_ID:
        	{
        		PackageManager linkedinManager = getPackageManager();
            	Intent linkedin=linkedinManager.getLaunchIntentForPackage("com.linkedin.android");
                startActivity(linkedin);
                break;
        	}
        	
        
        	
        	case BLOOMBERG_ID:
        	{
        		PackageManager bloomManager = getPackageManager();
            	Intent  bloom =  bloomManager.getLaunchIntentForPackage("com.bloomberg.android");
                startActivity( bloom);
                break;
        	}
        	
        	
        	
        	case SEED_ID:
        	{
        		PackageManager seedManager = getPackageManager();
            	Intent seed = seedManager.getLaunchIntentForPackage("");
                startActivity(seed);
                break;
        	}
        	

        	case BOATS_ID:
        	{
        		PackageManager boatsManager = getPackageManager();
            	Intent boats = boatsManager.getLaunchIntentForPackage("");
                startActivity(boats);
                break;
        	}
        		
        	}
        	
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
      if (keyCode == KeyEvent.KEYCODE_BACK) {
        if(browser.canGoBack()){
        	browser.goBack();
            return true;
        }
      }
      return super.onKeyDown(keyCode, event);
    }   
}